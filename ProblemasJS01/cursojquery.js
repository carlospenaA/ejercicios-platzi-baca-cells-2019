(function(){
    const API_WEATHER_KEY="80114c7878f599621184a687fc500a12";
    const API_WEATHER_URL = "https://api.openweathermap.org/data/2.5/weather?APPID="+ API_WEATHER_KEY+"&";
    const IMG_WEATHER="http://openweathermap.org/img/w/";
    
    var dia=new Date();
    var diaActual=dia.toLocaleDateString();
    var horaActual=dia.toLocaleTimeString();
    var $body=$('body');
    var nombreNuevaCiudad=$('[data-input="ctyAdd"]');
    var botonAgregar=$('[data-button="add"]');

    var climaCiudad={};
    climaCiudad.zone;
    climaCiudad.icon;
    climaCiudad.temp;
    climaCiudad.temp_max;
    climaCiudad.temp_min;
    climaCiudad.main;

    $(botonAgregar)

    if(navigator.geolocation){
        navigator.geolocation.getCurrentPosition(obtCordenadas, errorFounds);
    }else{
        alert('actualiza tu navegador');
    }

    function errorFounds(error){
        alert("Ocurrio un error: "+error.code);
    }

    function obtCordenadas(position){
        var latt= position.coords.latitude;
        var lon= position.coords.longitude;
        console.log("Tu posicion es: "+latt+", "+lon);
        $.getJSON(API_WEATHER_URL+"lat="+latt+"&lon="+lon, getCurrentWeather);
    }
    function getCurrentWeather(data){
        climaCiudad.zone= data.name;
    climaCiudad.icon=IMG_WEATHER+ data.weather[0].icon+".png";
    climaCiudad.temp=data.main.temp-273.15;
    climaCiudad.temp_max=data.main.temp_max-273.15;
    climaCiudad.temp_min=data.main.temp_min-273.15;
    climaCiudad.main=data.weather[0].main;
    console.log(data);
    renderTemplate();

        
    }

    function activateTemplate(id){
        var t = document.querySelector(id);
        return document.importNode(t.content, true);

    }

    function renderTemplate(){
        var clone = activateTemplate("#template--city");
        clone.querySelector('[data-dia]').innerHTML=diaActual;
        clone.querySelector('[data-time]').innerHTML=horaActual;
        clone.querySelector("[data-ciudad]").innerHTML=climaCiudad.zone;
        clone.querySelector("[data-icon]").src=climaCiudad.icon;
        clone.querySelector('[data-temp="max"]').innerHTML=climaCiudad.temp_max.toFixed(1);
        clone.querySelector('[data-temp="min"]').innerHTML=climaCiudad.temp_min.toFixed(1);
        clone.querySelector('[data-temp="current"]').innerHTML=climaCiudad.temp.toFixed(1);
        
        $(".cargando").hide();
        $($body).append(clone);
    }

})();