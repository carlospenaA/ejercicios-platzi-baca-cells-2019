

class MiMensaje extends HTMLElement{

    constructor(){
        super();
        this.addEventListener('click', function(e){
            alert('Click en el mensaje');
        });
        console.log('constructor: Cuando el elemento es creado');
    }

    static get observedAtrributes(){
        return ['msj','casi-visible']
    }

    connectedCallback(){
        console.log('connectedCallback: Cuando el elemento es insertado en el documento');
    }
    disconnectedCallback(){
        alert('addoptedCallback: Cuando el elemento es eliminado por otro documento');
    }
    adoptedCallback(){
        alert('adoptedCallback: Cuando el elemento es adoptado')
    }

    attributeChangedCallback(attrName, oldVal, newVal){
        console.log('attribute: Cuando cambia un atributo');
        if(attrName==='msj'){
            this.pintarMensaje(newVal);
        }
        if(attrName==='casi-visible'){
            this.setCasiVisible();
        }
    }
    pintarMensaje(msj){
        this.innerHTML=msj;
    }


    
    get msj(){
        return this.getAttribute('msj');
    }

    set msj(val){
        this.setAttribute('msj',val);
    }
    get casiVisible(){
        return this.hasAttribute('casi-visible');
    }

    set casiVisible(value){
        if(value){
            this.setAttribute('casi-visible','');
        }else{
            this.removeAttribute('casi-visible')
        }
    }

    setCasiVisible(){
        if(this.casiVisible){
            this.style.opacity=0.1;
        }else{
            this.style.opacity=1;
        }
    }
static get observedAtrributes(){
    return ['msj', 'casi-visible']

}    
}
customElements.define('mi-mensaje',MiMensaje);

let MiMensaje=document.createElement('mi-mensaje');
MiMensaje.msj='Otro mensaje';
document.body.appendChild(MiMensaje);

let tercerMensaje=new MiMensaje();
tercerMensaje.msj='Tercer mensaje';
document.body.appendChild(tercerMensaje);