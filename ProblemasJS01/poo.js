class Animal{
    constructor(){
        console.warn("nacio un pato");
    }
    hablar(){
        return 'cuak';
    }

}

class Clase{
    constructor(){
        this.name ='Luis';
        console.log('Constructor: '+this.name);
    }
    metodo(){
        console.log('Metodo: '+this.name);
    }
}

var c = new Clase();
console.log(c.name);
//c.metodo();

var pato=new Animal();
pato.hablar();

var donald = new Animal();
donald.hablar();