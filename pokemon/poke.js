
var imagenes=[];
imagenes["Cauchin"]="vaca.png";
imagenes["Pokacho"]="pollo.png";
imagenes["Tocinauro"]="cerdo.png";


class Pokemon{
    constructor(n,v,a){
        this.imagen=new Image();        
        this.vida=v;
        this.ataque=a;
        this.nombre=n;
        this.imagen.src=imagenes[this.nombre];
    }
    hablar(){
        alert("Esta chingadera dijo su nombre: "+this.nombre);
    }

    mostrar(){
        document.write("<strong>"+this.nombre+"</strong><br/>");
        document.body.appendChild(this.imagen);
        
        document.write("<br/>Vida: "+this.vida+"<br/>");
        document.write("Ataque: "+this.ataque+"<hr/>");
        
    }
}
var coleccion=[];
coleccion.push(new Pokemon("Cauchin",100,30));
coleccion.push(new Pokemon("Pokacho",80,50));
coleccion.push(new Pokemon("Tocinauro",120,40));

for(var individuos of coleccion){
    individuos.mostrar();
}

