
var caja = document.getElementById("caja_lineas");
var boton = document.getElementById("boton");
boton.addEventListener("click", dibujaClick);

var d=document.getElementById("dib");
var lienzo=d.getContext("2d");
var ancho=d.width;


function dibujarLinea(color, xinicial, yinicial, xfinal, yfinal){
lienzo.beginPath();
lienzo.strokeStyle=color;
lienzo.moveTo(xinicial,yinicial);
lienzo.lineTo(xfinal,yfinal);
lienzo.stroke();
lienzo.closePath();
}
function dibujaClick(){
    var num_lineas = parseInt(caja.value);
    console.log(caja);
    var lineas=num_lineas;
    var l=0;
    var yi, xf;
    var espacio = ancho/lineas;
    for(l=0;l<lineas;l++){
        yi=espacio*l;
        xf=espacio*(l+1);
        dibujarLinea("orange",0,yi,xf,300);
    
    
}
dibujarLinea("orange",1,1,1,299);
dibujarLinea("orange",1,299,299,299);
}
