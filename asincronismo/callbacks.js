const API_URL = 'https://swapi.co/api/';
const PEOPLE_URL = 'people/:id';

//const lukeUrl=`${API_URL}${PEOPLE_URL.replace(':id',1)}`;
const opts={crossDomain:true};

const onPersonajeResponse = function(personaje){
    console.log(`Hola yo soy ${personaje.name}`); 
 }

function obtenerPersonaje(id){
    return new Promise((resolve, reject)=>{
    const url=`${API_URL}${PEOPLE_URL.replace(':id',id)}`;
    $.get(url, opts, function(data){
        resolve(data)}).fail(()=>reject(id))
})
    
}

function onError(id){
    console.log(`Sucedio un error al obtener el personaje ${id}`)
}
obtenerPersonaje(1)
.then(personaje=>{
    console.log(`El personaje 1 es: ${personaje.name}`)
    return obtenerPersonaje(2)
})
.then(personaje=>{
    console.log(`El personaje 2 es: ${personaje.name}`)
    return obtenerPersonaje(3)
})
.then(personaje=>{
    console.log(`El personaje 3 es: ${personaje.name}`)
    return obtenerPersonaje(4)
})
.then(personaje=>{
    console.log(`El personaje 4 es: ${personaje.name}`)
    
})
.catch(onError)

obtenerPersonaje(1, function(){
    console.log(`Hola, yo soy ${personaje.name}`);
});

async function obtenerPersonajes(){
    var ids = [1,2,3,4,5,6,7];
    var promesas = ids.map(id=>obtenerPersonaje(id));
    try{
        var personajes= await Promise.all(promesas);
        console.log(personajes);
    }catch(id){
        onError(id);
    }
    

    /*Promise
    .all(promesas)
    .then(personajes=>console.log(personajes))
    .catch(onError)*/
}
obtenerPersonajes();


/*var settings = {
	"async": true,
	"crossDomain": true,
	"url": "https://montanaflynn-fifa-world-cup.p.rapidapi.com/teams",
	"method": "GET",
	"headers": {
		"x-rapidapi-host": "montanaflynn-fifa-world-cup.p.rapidapi.com",
		"x-rapidapi-key": "SIGN-UP-FOR-KEY",
		"accepts": "json"
	}
}

$.ajax(settings).done(function (response) {
	console.log(response);
});*/